#!/bin/bash

sudo apt-get update

sudo apt-get install -y php5 apache2 php5-mcrypt php5-cli  libapache2-mod-php5 git 

cp /vagrant/config/apache2.conf /etc/apache2/apache2.conf -rf

cp /vagrant/config/000-default.conf /etc/apache2/sites-available/000-default.conf -rf

curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer

rm -rf /var/www

ln -fs /vagrant /var/www

sudo /etc/init.d/apache2 restart


